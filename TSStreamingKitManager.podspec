
Pod::Spec.new do |s|

s.name         = "TSStreamingKitManager"
s.version      = "1.0.0"
s.summary      = "Wraps the pod StreamKit providing access to playing audio files"
s.author       = "Richard Moult"
s.homepage     = "https://rmoult@bitbucket.org/rmoult/tsstreamingkitmanager"
s.license      = { :type => 'The MIT License (MIT)', :text => <<-LICENSE
Copyright (c) 2015 Richard Moult.
LICENSE
}
s.source_files = 'TSStreamingKitManager/PodClasses/*.{h,m}'
s.requires_arc = true
s.platform     = :ios, '7.0'
s.source       = {:git => "https://rmoult@bitbucket.org/rmoult/tsstreamingkitmanager.git", :tag=> '1.0.0'}
s.dependency 'StreamingKit'

end
