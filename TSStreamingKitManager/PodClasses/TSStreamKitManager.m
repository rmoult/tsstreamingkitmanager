//
//  Created by Richard Moult on 15/06/2014.
//  Copyright (c) 2014 TrickySquirrel. All rights reserved.
//

#import "TSStreamKitManager.h"
#import <AVFoundation/AVFoundation.h>
#import <StreamingKit/STKAudioPlayer.h>
#import <StreamingKit/STKDataSource.h>



@interface TSStreamKitManager() <STKAudioPlayerDelegate>
@property (nonatomic, strong) STKAudioPlayer * audioPlayer;
@property (nonatomic, strong) NSTimer * timer;
@property (nonatomic, assign) BOOL isAlreadyBuffering;
@end



@implementation TSStreamKitManager


- (id)init {
    
    self = [super init];
    
    if (self) {
        
        self.isAlreadyBuffering = NO;
        
        self.audioPlayer = [self createAudioPlayer];
        
        self.audioPlayer.delegate = self;
    }
    return self;
}


- (STKAudioPlayer *)createAudioPlayer {
    
    STKAudioPlayer * audioPlayer = [[STKAudioPlayer alloc] initWithOptions:(STKAudioPlayerOptions){ .flushQueueOnSeek = YES, .enableVolumeMixer = NO, .equalizerBandFrequencies = {50, 100, 200, 400, 800, 1600, 2600, 16000} }];
    
	audioPlayer.meteringEnabled = YES;
	audioPlayer.volume = 1;
    
    return audioPlayer;
}




#pragma mark - audio controls

- (void)playWithURL:(NSURL *)URL {
    
    [self startAudioProgressTimer];
    
    [self.audioPlayer playURL:URL];
}


- (void)seekToTime:(CGFloat)value {
    
    [self.audioPlayer seekToTime:value];
}

- (void)pause {
    
    [self.audioPlayer pause];
}


- (void)resume {
    
    [self.audioPlayer resume];
}


- (void)stop {
    
    [self stopAudioProgressTimer];
    [self.audioPlayer stop];
}


- (STKAudioPlayerState)playerState {
    
    return self.audioPlayer.state;
}


#pragma mark - timer

- (void)startAudioProgressTimer {
    
    [self stopAudioProgressTimer];
    self.timer = [NSTimer timerWithTimeInterval:0.001 target:self selector:@selector(tick) userInfo:nil repeats:YES];
	[[NSRunLoop currentRunLoop] addTimer:self.timer forMode:NSRunLoopCommonModes];
}


- (void)stopAudioProgressTimer {
    
    [self.timer invalidate];
    self.timer = nil;
}


- (void)tick {
    
    [self updateDurationDelegate];
}


#pragma mark - fire delegates

- (void)updateDurationDelegate {
    
    if (self.audioPlayer.duration != 0) {
        
        [self.delegate streamKitManager:self playingDuration:self.audioPlayer.progress maxDuration:self.audioPlayer.duration];
    }
}


- (void)updateBufferDelegateOnChange {
    
    BOOL buffering = self.audioPlayer.state == STKAudioPlayerStateBuffering ? YES : NO;
    if (buffering != self.isAlreadyBuffering) {
        self.isAlreadyBuffering = buffering;
        [self.delegate streamKitManager:self isBuffering:buffering];
    }
}


#pragma mark - audio player delgate

/// Raised when an item has started playing
- (void) audioPlayer:(STKAudioPlayer*)audioPlayer didStartPlayingQueueItemId:(NSObject*)queueItemId {}

/// Raised when an item has finished buffering (may or may not be the currently playing item)
/// This event may be raised multiple times for the same item if seek is invoked on the player

- (void) audioPlayer:(STKAudioPlayer*)audioPlayer didFinishBufferingSourceWithQueueItemId:(NSObject*)queueItemId {}
/// Raised when the state of the player has changed

- (void) audioPlayer:(STKAudioPlayer*)audioPlayer stateChanged:(STKAudioPlayerState)state previousState:(STKAudioPlayerState)previousState {

    [self updateBufferDelegateOnChange];
}


/// Raised when an item has finished playing
- (void) audioPlayer:(STKAudioPlayer*)audioPlayer didFinishPlayingQueueItemId:(NSObject*)queueItemId withReason:(STKAudioPlayerStopReason)stopReason andProgress:(double)progress andDuration:(double)duration {
    
    [self.delegate streamKitManagerDidReachEndOfTrack:self reason:stopReason];
}

/// Raised when an unexpected and possibly unrecoverable error has occured (usually best to recreate the STKAudioPlauyer)
- (void) audioPlayer:(STKAudioPlayer*)audioPlayer unexpectedError:(STKAudioPlayerErrorCode)errorCode {
    
    NSError * error = [self errorWithAudioPlayerErrorCode:errorCode];
    if (error) {
        [self.delegate streamKitManager:self error:error];
    }
}


#pragma mark - error handling

- (NSError *)errorWithAudioPlayerErrorCode:(STKAudioPlayerErrorCode)errorCode {
    
    switch (errorCode) {
            
        case STKAudioPlayerErrorStreamParseBytesFailed:
        case STKAudioPlayerErrorCodecError:
            return [self streamKitErrorWithCode:errorCode locStringKey:kStreamKitManagerLocStringCodecErrorKey];

        case STKAudioPlayerErrorDataNotFound:
            return [self streamKitErrorWithCode:errorCode locStringKey:kStreamKitManagerLocStringNoDataErrorKey];

        case STKAudioPlayerErrorDataSource:
            return [self streamKitErrorWithCode:errorCode locStringKey:kStreamKitManagerLocStringDataSourceErrorKey];
            
        case STKAudioPlayerErrorAudioSystemError:
        case STKAudioPlayerErrorOther:
            return [self streamKitErrorWithCode:errorCode locStringKey:kStreamKitManagerLocStringGeneralErrorKey];
            
        default:
            break;
    }
    NSLog(@"%s unexpected error occured ignorning %d", __PRETTY_FUNCTION__, errorCode);
    return nil;
}


- (NSError *)streamKitErrorWithCode:(STKAudioPlayerErrorCode)errorCode locStringKey:(NSString *)locStringKey {
 
    NSString * description = NSLocalizedString(locStringKey, nil);
    
    return [NSError errorWithDomain:@"StreamKitManagerDomain" code:errorCode userInfo:@{NSLocalizedDescriptionKey: description}];
}


@end
